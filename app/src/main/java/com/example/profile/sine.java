package com.example.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class sine extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sine);
    }

    public void Clickprofile(View view) {
        Intent intent = new Intent(sine.this, MainActivity.class);
        startActivity(intent);
    }

    public void email(View view) {
        Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse("https://mail.google.com/mail/u/0/?tab=rm#inbox"));
        startActivity(intent);
    }

    public void facebook(View view) {
        Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.facebook.com/profile.php?id=100003915421341"));
        startActivity(intent);
    }

    public void tagream(View view) {
        Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/sine_2105/"));
        startActivity(intent);

    }
}